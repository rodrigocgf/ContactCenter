#pragma once


// DialogSip dialog

class DialogSip : public CDialog
{
	DECLARE_DYNAMIC(DialogSip)

public:
	DialogSip(CWnd* pParent = NULL);   // standard constructor
	virtual ~DialogSip();

// Dialog Data
	enum { IDD = IDD_FORMVIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnStart();
public:
	afx_msg void OnBnClickedBtnStop();
};
