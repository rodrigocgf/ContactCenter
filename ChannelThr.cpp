#include "StdAfx.h"
#include "ChannelThr.h"
#include "sqlite\CppSqlite3.h"


ChannelThr::ChannelThr(	boost::shared_ptr< CPluginSip >	pluginSipPtr , 
						std::string dbName ,						
						std::string ramal,
						std::string	id,
						std::string grupoCanalID
						)	:  m_bStop(false)
{	
	m_strRamal = ramal;
	m_strCanalID = id;
	m_strGrupoCanalID = grupoCanalID;
	
	m_dbPtr.reset( (IDatabase *)( new CppSQLite3DB() ) );
	m_dbPtr->Open(dbName.c_str());		

	m_pluginSipPtr = pluginSipPtr;
}

ChannelThr::ChannelThr(	ISipServer * parent , 
						boost::shared_ptr< CPluginSip >	pluginSipPtr,
						std::string dbName ,						
						std::string ramal,
						std::string	id,
						std::string grupoCanalID
					)	: m_bStop(false)
{	
	m_strRamal = ramal;
	m_strCanalID = id;
	m_strGrupoCanalID = grupoCanalID;
	
	m_dbPtr.reset( (IDatabase *)( new CppSQLite3DB() ) );
	m_dbPtr->Open(dbName.c_str());		

	m_pSipServer = parent;	
	m_pluginSipPtr = pluginSipPtr;
}

ChannelThr::ChannelThr(void)
{
	
}

ChannelThr::~ChannelThr(void)
{
}



void ChannelThr::Init()
{
	
}

void ChannelThr::Start()
{	
	CarregaCalendarioCanais();
	CarregaGruposCanais();
	DeterminaPerfilID();
	DeterminaScriptID();
	CarregaBlocoInicial();

	pj_status_t rc;	
	unsigned flags = 0;	

	m_pool = PoolCreate("ChannelThr Pool", 10000);
	rc = ThreadCreate(	m_pool , "thread", (pj_thread_proc*)&ChannelThr::Run,										
										this,
										PJ_THREAD_DEFAULT_STACK_SIZE,
										flags,
										&m_thread);
	
	rc = ResumeThread(m_thread);
}

void ChannelThr::SetCallInfo(pjsua_call_info call_info)
{
	m_callInfo = call_info;
}

void ChannelThr::NotifyAll()
{	
	//m_condWaitIncommingCall.notify_all();		
	m_condWaitIncommingCall.notify_one();
}

pj_pool_t *	ChannelThr::PoolCreate(std::string strName, int size)
{	
	return m_pluginSipPtr->PoolCreate(const_cast<char *>(strName.c_str() ), size);
}

pj_status_t	ChannelThr::ThreadCreate(	pj_pool_t	* pool , 
										std::string strName , 
										pj_thread_proc *proc, 
										void *arg, pj_size_t stack_size, 
										unsigned flags,
										pj_thread_t **ptr_thread)
{
	return m_pluginSipPtr->ThreadCreate(pool, strName, proc, arg, stack_size, flags, ptr_thread);
}

pj_status_t	ChannelThr::ResumeThread(pj_thread_t *ptr_thread)
{
	return m_pluginSipPtr->ResumeThread(ptr_thread);
}

PJ_DEF(pj_status_t) ChannelThr::ThreadRegister ( std::string strThreadName, pj_thread_desc desc, pj_thread_t **ptr_thread)
{
	return m_pluginSipPtr->ThreadRegister (strThreadName , desc , ptr_thread );
}

void ChannelThr::ThreadDestroy()
{	
	m_pluginSipPtr->ThreadDestroy(m_thread);
	//m_pluginSipPtr->hangup_call(m_callInfo);
}

///<summary>
///	Main thread loop
///</summary>
void ChannelThr::Run(ChannelThr * pThis)
{		
	std::string strTool;	

	pj_thread_desc		desc;
	pj_thread_t			* this_thread;	
	unsigned			id;
	pj_status_t			rc;

	pj_bzero(desc, sizeof(desc));
	pThis->ThreadRegister("thread ChannelThr", desc, &this_thread);		
	
	do
	{		
		strTool = pThis->RecuperaTool(pThis->m_strBlocoID);
		if ( !strTool.empty() )
			pThis->AnalisaTool(strTool);
		else
		{
			pThis->m_pluginSipPtr->hangup_call(pThis->m_callInfo);

			pThis->DeterminaPerfilID();
			pThis->DeterminaScriptID();
			pThis->CarregaBlocoInicial();
		}
		
		//boost::mutex::scoped_lock lk(  m_mutex );		
		//m_cond.wait( lk );	
		//lk.unlock();		
	} while ( !pThis->m_bStop );	
	//} while ( !pThis->m_bStop && !strTool.empty() );	

	
	pThis->ThreadDestroy();
}

CInfoCanal ChannelThr::DetInfoCanalByCallID(int paramCallID)
{
	canal_info_by_callID& callID_index = infoCanalSet.get<callID>();
	canal_info_by_callID::iterator it = callID_index.find(paramCallID);

	CInfoCanal canalInfo = (*it);
	return canalInfo;
}

///<summary>
/// TODO : Recuperar futuramente de uma tabela no banco de dados
///</summary>

CInfoCanal ChannelThr::DetInfoCanalByRamal(std::string Ramal)
{
	canal_info_by_ramal& ramal_index = infoCanalSet.get<ramal>();
	canal_info_by_ramal::iterator it = ramal_index.find(Ramal);

	CInfoCanal canalInfo = (*it);
	return canalInfo;
}

void ChannelThr::Stop()
{
	//m_cond.notify_all();
	m_cond.notify_one();
	m_bStop = true;
	m_ChannelThreadPtr->join();
}

std::string ChannelThr::RecuperaTool(std::string strBlocoID)
{	
	std::string strTool;
	std::string sql = boost::str(boost::format("select Tools.Tool from Tools INNER JOIN Blocos ON Tools.ToolID = Blocos.ToolID WHERE Blocos.ScriptID = %s and Blocos.BlocoID = %s") % m_strScriptIDAtual % strBlocoID );
	boost::shared_ptr<DataTable> dt = m_dbPtr->ExecuteQuery(sql.c_str());

	for (	DataTable::iterator it = dt->mi_dataRow.get<row>().begin() ; 
			it != dt->mi_dataRow.get<row>().end() ; 
			it++ )
	{
		DataRow row = (*it);		

		for (	DataRow::iterator itC = row.mi_dataColumn.get<column>().begin() ; 
				itC != row.mi_dataColumn.get<column>().end() ; 
				itC++ )
		{
			DataColumn column = (*itC);
			strTool = column.value;
		}
	}

	return strTool;
}

///<summary>
/// Carrega o BlocoInicial da tabela Scripts
///</summary>
//std::string ChannelThr::CarregaBlocoInicial()
void ChannelThr::CarregaBlocoInicial()
{
	int iBlocoID = 0;
	std::string sql = boost::str(boost::format("Select BlocoInicial from Scripts where ScriptID = %s") % m_strScriptIDAtual );
	iBlocoID = m_dbPtr->ExecuteScalar(sql.c_str());
	m_strBlocoID = boost::lexical_cast<std::string>(iBlocoID);
	return;	
}

///<summary>
/// Determina o ScriptID da tabela Intervalos
/// A cada 15 minutos o intervalo � incrementado de 1
///</summary>
void ChannelThr::DeterminaScriptID()
{
	int hora = 0 , minuto = 0;	
	int intervalo = 0;

	time_t rawtime;
    tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

	hora = timeinfo->tm_hour;
	minuto = timeinfo->tm_min;

	intervalo = hora*4 + (int)(minuto/15);

	std::string sql = boost::str(	boost::format("Select ScriptID from Intervalos where PerfilID = '%s' and InxInic <= %d and InxFim >= %d") % 
									m_strPerfilIDAtual.c_str() % 
									intervalo % 
									intervalo);
	boost::shared_ptr<DataTable> dt = m_dbPtr->ExecuteQuery(sql.c_str());

	for (	DataTable::iterator it = dt->mi_dataRow.get<row>().begin() ; 
			it != dt->mi_dataRow.get<row>().end() ; 
			it++ )
	{
		DataRow row = (*it);		

		for (	DataRow::iterator itC = row.mi_dataColumn.get<column>().begin() ; 
				itC != row.mi_dataColumn.get<column>().end() ; 
				itC++ )
		{
			DataColumn column = (*itC);
			m_strScriptIDAtual = column.value;
		}
	}
	
}

///<summary>
/// Determina o perfilID do dia da semana dentre os da estrutura ST_PERFIL_DIAS_DA_SEMANA
///<summary>
void ChannelThr::DeterminaPerfilID()
{
	time_t rawtime;
    tm * timeinfo;
    time(&rawtime);
    timeinfo=localtime(&rawtime);
    
    switch ( timeinfo->tm_wday )
	{
	case SEGUNDA:
		m_strPerfilIDAtual = m_PerfilDaSemana.perfil_segunda;
		break;
	case TERCA:
		m_strPerfilIDAtual = m_PerfilDaSemana.perfil_terca;
		break;
	case QUARTA:
		m_strPerfilIDAtual = m_PerfilDaSemana.perfil_quarta;
		break;
	case QUINTA:
		m_strPerfilIDAtual = m_PerfilDaSemana.perfil_quinta;
		break;
	case SEXTA:
		m_strPerfilIDAtual = m_PerfilDaSemana.perfil_sexta;
		break;
	case SABADO:
		m_strPerfilIDAtual = m_PerfilDaSemana.perfil_sabado;
		break;
	case DOMINGO:
		m_strPerfilIDAtual = m_PerfilDaSemana.perfil_domingo;
		break;
	}
}

///<summary>
/// Carrega os perfis por dia da semana
///</summary>
void ChannelThr::CarregaGruposCanais()
{
	std::string sqlAux;
	sqlAux.append("Select Segunda, Terca, Quarta, Quinta, Sexta, Sabado , ");
	sqlAux.append("Domingo from GruposCanais where GrupoCanalID = %s");
	std::string sql = boost::str(boost::format(sqlAux.c_str()) % m_strGrupoCanalID.c_str() );	
	boost::shared_ptr<DataTable> dt = m_dbPtr->ExecuteQuery(sql.c_str());

	for (	DataTable::iterator it = dt->mi_dataRow.get<row>().begin() ; 
			it != dt->mi_dataRow.get<row>().end() ; 
			it++ )
	{
		DataRow row = (*it);		

		for (	DataRow::iterator itC = row.mi_dataColumn.get<column>().begin() ; 
				itC != row.mi_dataColumn.get<column>().end() ; 
				itC++ )
		{
			DataColumn column = (*itC);

			if ( !column.name.compare("Segunda"))
			{
				if ( CalendarioCanaisContains( column.value, m_calendarioCanaisList ) )
					m_PerfilDaSemana.perfil_segunda = column.value;
			}
			else if ( !column.name.compare("Terca"))
			{
				if ( CalendarioCanaisContains( column.value, m_calendarioCanaisList ) )
					m_PerfilDaSemana.perfil_terca = column.value;
			}
			else if ( !column.name.compare("Quarta"))
			{
				if ( CalendarioCanaisContains( column.value, m_calendarioCanaisList ) )
					m_PerfilDaSemana.perfil_quarta = column.value;			
			}
			else if ( !column.name.compare("Quinta"))
			{
				if ( CalendarioCanaisContains( column.value, m_calendarioCanaisList ) )
					m_PerfilDaSemana.perfil_quinta = column.value;
			}
			else if ( !column.name.compare("Sexta"))
			{
				if ( CalendarioCanaisContains( column.value, m_calendarioCanaisList ) )
					m_PerfilDaSemana.perfil_sexta = column.value;
			}
			else if ( !column.name.compare("Sabado"))
			{
				if ( CalendarioCanaisContains( column.value, m_calendarioCanaisList ) )
					m_PerfilDaSemana.perfil_sabado = column.value;
			}
			else if ( !column.name.compare("Domingo"))
			{
				if ( CalendarioCanaisContains( column.value, m_calendarioCanaisList ) )
					m_PerfilDaSemana.perfil_domingo = column.value;			
			}
		}
	}
}

bool ChannelThr::CalendarioCanaisContains(std::string strPerfilToFind , std::list<ST_CALENDARIOCANAIS> calendarioCanaisList )
{
	bool bRet = false;

	std::list<ST_CALENDARIOCANAIS>::const_iterator pos;

    for(pos = calendarioCanaisList.begin(); pos != calendarioCanaisList.end(); ++pos)
	{
		ST_CALENDARIOCANAIS stLoop = (*pos);
		if ( pos->PerfilID.compare( strPerfilToFind ) )
		{
			bRet = true;
			break;
		}
	}

	return bRet;
}
std::string ChannelThr::Today()
{
	time_t t = time(0);
    struct tm * now = localtime( & t );	
	std::string strToday = boost::str(boost::format("%04d/%02d/%02d") % (now->tm_year + 1900) % (now->tm_mon+1) % (now->tm_mday) );
	return strToday;
}

///<summary>
/// Seleciona os conjuntos { Dia , GrupoCanalID, PerfilID }
/// da tabela CalendarioCanais relativos ao dia atual.
///</summary>
void ChannelThr::CarregaCalendarioCanais()
{		
	std::string strToday = Today();

	std::string sql = "select Dia, GrupoCanalID, PerfilID from CalendarioCanais";
	boost::shared_ptr<DataTable> dt = m_dbPtr->ExecuteQuery(sql.c_str());

	for (	DataTable::iterator it = dt->mi_dataRow.get<row>().begin() ; 
			it != dt->mi_dataRow.get<row>().end() ; 
			it++ )
	{
		DataRow row = (*it);		
		ST_CALENDARIOCANAIS stCalendarioCanais;

		std::string LoopDia;
		std::string LoopGrupoCanalID;
		std::string LoopPerfilID;

		for (	DataRow::iterator itC = row.mi_dataColumn.get<column>().begin() ; 
				itC != row.mi_dataColumn.get<column>().end() ; 
				itC++ )
		{
			DataColumn column = (*itC);

			if ( !column.name.compare("Dia"))
				LoopDia = column.value;				
			else if ( !column.name.compare("GrupoCanalID"))
				LoopGrupoCanalID = column.value;				
			else if ( !column.name.compare("PerfilID"))
				LoopPerfilID = column.value;
		

			if ( !LoopDia.compare(0,10,strToday) && !LoopGrupoCanalID.empty() && !LoopPerfilID.empty() )
			{
				stCalendarioCanais.Dia = LoopDia;
				stCalendarioCanais.GrupoCanalID = LoopGrupoCanalID;
				stCalendarioCanais.PerfilID = LoopPerfilID;

				m_calendarioCanaisList.push_back(stCalendarioCanais);
			}			
		}
	}
}

void ChannelThr::AnalisaTool(std::string strTool)
{
	if ( !strTool.compare("Incomming") )
	{
		boost::mutex::scoped_lock lk(  m_mutex );		
		m_condWaitIncommingCall.wait( lk );	
		lk.unlock();		
		
		AnswerCall();

		//std::string strCallId = boost::lexical_cast<std::string>(m_callInfo.id);
		//PJ_LOG(	3,(THIS_FILE, " == > Answering Call : Ramal %s --- Call ID %s", m_strRamal.c_str() , strCallId.c_str() ) );

		// Supondo que ocorreu um PRX, pega-se o valor de DesvioNext
		DeterminaProximoBloco(strTool);
	}
	else if ( !strTool.compare("Play") )
	{		
		m_GroupFilesSet.clear();
		CarregaGroupFiles();		
		
		SetWaitForPlays( m_GroupFilesSet.size() );

		for ( GROUPFILES_SET::iterator it = m_GroupFilesSet.begin();
			it != m_GroupFilesSet.end();
			it++ )
		{
			GROUPFILES gf = (*it);
			int iOrdemGrupo = gf.ordemGrupo;
			int iOrdemArquvio = gf.ordemArquivo;			
			PlayFile(m_strCanalID , m_strRamal , gf.fullFilePathName);
		}

		//boost::mutex::scoped_lock lk1(  m_mutex );		
		//m_condWailPlayEnd.wait( lk1 );	
		//lk1.unlock();

		// Supondo que ocorreu um PRX, pega-se o valor de DesvioNext
		DeterminaProximoBloco(strTool);
		
	}
	else if ( !strTool.compare("Record") )
	{
		CarregaFromRecord();


		RecordFile(		m_strCanalID , 
						m_strRamal , 
						"C:\\Users\\rodrigof\\Projects\\URA\\Tags\\CONTACT_CENTER_CPP_VS2005\\Debug\\Som\\teste123.wav",
						m_strTempoGravacao,
						m_strTonsTerminacao,
						m_strSilencioMax);

		//boost::mutex::scoped_lock lk(  m_mutex );		
		//m_condWaitRecordEnd.wait( lk );	
		//lk.unlock();

		DeterminaProximoBloco(strTool);
		int dbg1 = 1;
	}
	else if ( !strTool.compare("GetDigits") )
	{
		DeterminaProximoBloco(strTool);
		int dbg1 = 1;
	}
	else if ( !strTool.compare("HangUp") )
	{
		int dbg1 = 1;
	}
}

void ChannelThr::AnswerCall()
{	
	m_pluginSipPtr->answer_call(m_callInfo);
}

void ChannelThr::SetWaitForPlays(int num)
{	
	m_pluginSipPtr->SetWaitForPlays(num);
}

void ChannelThr::PlayFile(std::string strCanalID , std::string strRamal , std::string fileToPlay)
{	
	bool found = false;
	pjsua_call_info call_info;

	found = m_pSipServer->FindCallInfo(strRamal, call_info);

	if ( found )
		m_pluginSipPtr->play_file(strCanalID, call_info , fileToPlay);
}

void  ChannelThr::RecordFile(	std::string strCanalID , 
								std::string strRamal ,
								std::string strFileFullPathName,
								std::string strTempoGravacao,
								std::string strTonsTerminacao,
								std::string strSilencioMax)
{
	bool found = false;
	pjsua_call_info call_info;
	found = m_pSipServer->FindCallInfo(strRamal, call_info);	

	if ( found )
		m_pluginSipPtr->record_file(	
									strCanalID,
									call_info,
									strFileFullPathName,
									strTempoGravacao,
									strTonsTerminacao,
									strSilencioMax);
}

void ChannelThr::CarregaFromRecord()
{
	std::string sql;
	sql.append("Select TempoGravacao, TonsTerminacao, Variavel, SilencioMax from Record ");
	sql.append(" where BlocoID = ");
	sql.append(m_strBlocoID);
	boost::shared_ptr<DataTable> dt = m_dbPtr->ExecuteQuery(sql.c_str());

	for (	DataTable::iterator it = dt->mi_dataRow.get<row>().begin() ; 
			it != dt->mi_dataRow.get<row>().end() ; 
			it++ )
	{
		DataRow row = (*it);	
		GROUPFILES gf;

		for (	DataRow::iterator itC = row.mi_dataColumn.get<column>().begin() ; 
				itC != row.mi_dataColumn.get<column>().end() ; 
				itC++ )
		{
			DataColumn coluna = (*itC);	
			if ( !coluna.name.compare("TempoGravacao") )
				m_strTempoGravacao = coluna.value;
			else if ( !coluna.name.compare("TonsTerminacao") )
				m_strTonsTerminacao = coluna.value;
			else if ( !coluna.name.compare("Variavel") )
				m_strVariavel = coluna.value;
			else if ( !coluna.name.compare("SilencioMax") )
				m_strSilencioMax = coluna.value;
		}
	}
}

void ChannelThr::CarregaGroupFiles()
{
	std::string sql;
	sql.append("Select G.Ordem as OrdemGrupo, F.Ordem as OrdemArquivo, F.Dados from FilesPlay F inner join ");
	sql.append(" GruposFilesPlay G on  F.GrupoID = G.GrupoID  and  F.BlocoID = G.BlocoID where G.BlocoID = ");
	sql.append(m_strBlocoID);
	boost::shared_ptr<DataTable> dt = m_dbPtr->ExecuteQuery(sql.c_str());

	
	for (	DataTable::iterator it = dt->mi_dataRow.get<row>().begin() ; 
			it != dt->mi_dataRow.get<row>().end() ; 
			it++ )
	{
		DataRow row = (*it);	
		GROUPFILES gf;

		for (	DataRow::iterator itC = row.mi_dataColumn.get<column>().begin() ; 
				itC != row.mi_dataColumn.get<column>().end() ; 
				itC++ )
		{
			DataColumn coluna = (*itC);			

			if ( !coluna.name.compare("OrdemGrupo") )
				gf.ordemGrupo = boost::lexical_cast<int>(coluna.value);
			else if ( !coluna.name.compare("OrdemArquivo") )
				gf.ordemArquivo = boost::lexical_cast<int>(coluna.value);
			else if ( !coluna.name.compare("Dados") )
				gf.fullFilePathName = coluna.value;

			if ( itC  == ( row.mi_dataColumn.get<column>().end() - 1 ) )			
				m_GroupFilesSet.insert(gf);			
		}
	}

}

void ChannelThr::DeterminaProximoBloco(std::string strTool)
{
	std::string sql = boost::str(boost::format("Select DesvioNext from %s where BlocoID = %s") % strTool % m_strBlocoID );
	int iDesvioNext =  m_dbPtr->ExecuteScalar(sql.c_str());
	m_strBlocoID = boost::lexical_cast<std::string>(iDesvioNext);
}

void ChannelThr::SetDtmf(int dtmf)
{
	char chAscii = (char)(dtmf);
	m_Dtmf = boost::lexical_cast<std::string>(chAscii);
}


