#pragma once
#include "stdafx.h"
#include "sqlite\sqlite3.h"

class CppSQLite3Query;

using boost::multi_index_container;
using namespace boost::multi_index;

struct DataColumn
{
	DataColumn() { }
	DataColumn( std::string nameP , std::string valueP ) { name = nameP; value = valueP; }
	std::string name;
	std::string value;
};

struct row{};
struct column{};
struct id{};
struct name{};
struct value{};
struct number{};

typedef boost::multi_index_container
<
	DataColumn,
	indexed_by
	<
		random_access<tag<column> >,
		ordered_unique<tag<name>, member<DataColumn, std::string, &DataColumn::name> >		
	>
> TDataColumn;


class DataRow
{
public:
	DataRow() {}
	int				rowNumber;
	TDataColumn		mi_dataColumn;

	typedef TDataColumn::index<column>::type TColumnIndex;
	typedef TColumnIndex::iterator iterator;
};

typedef boost::multi_index_container
<
	DataRow,
	indexed_by
	<
		random_access<tag<row> >		
		//ordered_unique<tag<number>, member<DataRow, int, &DataRow::rowNumber> >		
	>
> TDataRow;

class DataTable
{
public:
	DataTable() {}
	TDataRow mi_dataRow;
	typedef TDataRow::index<row>::type TRowIndex;
	typedef TRowIndex::iterator iterator;
	unsigned size() const { return mi_dataRow.size(); }
	int numFields()
	{	
		if ( size() > 0)
		{
			iterator it = mi_dataRow.begin();
			DataRow row = (*it);
			return row.mi_dataColumn.size();
		}
		else
			return 0;
	}
};


class IDatabase
{
public:
	IDatabase(){}
	virtual ~IDatabase(){}

	virtual void							Open(const char * szFile)=0;
	virtual void							Close()=0;
	virtual int								Execute(const char * sql)=0;
	virtual int								ExecuteScalar(const char * sql)=0;
	virtual boost::shared_ptr<DataTable>	ExecuteQuery(const char * query)=0;
	virtual bool							tableExists(const char* szTable)=0;	

	std::string					m_ConnectionString;
};