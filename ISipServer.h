#pragma once 

#include "stdafx.h"

class ISipServer
{
public:
	//virtual pj_pool_t * PoolCreate(std::string strName, int size) = 0;
	/*virtual pj_status_t		ThreadCreate(	pj_pool_t	* pool , std::string strName , pj_thread_proc *proc, 
											void *arg, pj_size_t stack_size, 
											unsigned flags,
											pj_thread_t **ptr_thread) = 0;*/
	//virtual pj_status_t	    ResumeThread(pj_thread_t *ptr_thread) = 0;
	//virtual PJ_DEF(pj_status_t) ThreadRegister ( std::string strThreadName, pj_thread_desc desc, pj_thread_t **ptr_thread) = 0;
	virtual std::string GetDtmf() = 0;
	virtual void ClearDtmf() = 0;
	virtual void OnIncomingCall(pjsua_call_info callInfo) = 0;
	//virtual void AnswerCall(pjsua_call_info canalInfo) = 0;
	virtual bool FindCallInfo(std::string strRamal, pjsua_call_info & call_info) = 0;
	//virtual void OnIncomingCall(std::string Ramal) = 0;
	virtual void OnPlayEof(pjmedia_port *port, std::string Ramal) = 0;
	virtual void OnDtmf(pjsua_call_id call_id, int dtmf) = 0;
	//virtual void RecordFile(std::string strCanalID, std::string strRamal, std::string strFileFullPathName, std::string strTempoGravacao, std::string strTonsTerminacao, std::string strSilencioMax) = 0;
	//virtual void PlayFile(std::string strCanalID, std::string strRamal , std::string fileToPlay) = 0;
	//virtual void SetWaitForPlays(int num) = 0;
};