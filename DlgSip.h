#pragma once
#include "stdafx.h"
#include "MainThr.h"

class DlgSip
{
public:
	boost::shared_ptr< MainThr > m_mainThrPtr;			

	DlgSip(void);
	~DlgSip(void);
	int Show();	
};
