#include "StdAfx.h"
#include "SipServer.h"
#include "sqlite\CppSqlite3.h"

SipServer::SipServer(void)
{
	
}

SipServer::SipServer(std::string dbName)
{
	m_strDbFullPathName = dbName;
}

SipServer::~SipServer(void)
{
	/*
	destroy_sip();
	destroy_media();

	if (app.pool) 
	{
		pj_pool_release(app.pool);
		app.pool = NULL;
		pj_caching_pool_destroy(&app.cp);
	}

	app_logging_shutdown();

	// Shutdown PJLIB 
	pj_shutdown();
	*/
}

int SipServer::Start()
{	
	m_dbPtr.reset( (IDatabase *)( new CppSQLite3DB() ) );
	m_dbPtr->Open(m_strDbFullPathName.c_str());	
	
	GetParametrosGerais();
	int numCanais = GetNumCanaisHabilitados();	
	LaunchChannelsThreads(numCanais);
	return 0;
}

void SipServer::LaunchChannelsThreads(int numCanais)
{
	boost::shared_ptr<ChannelThr> channelThrPtr;
	std::string strId;
	std::string strRegistrar;
	std::list<ST_CANAIS>::const_iterator pos;
	std::list<ST_CANAIS> canaisList = RecuperaCanais();
	
	m_pluginSipPtr.reset( new CPluginSip(this) );	
	m_pluginSipPtr->app_init();	
	m_pluginSipPtr->pj_dump_config();

	int canalID = 1;
	for(pos = canaisList.begin(); pos != canaisList.end(); ++pos)
	{
		ST_CANAIS stCanais = (*pos);
		std::string strCanal = stCanais.Canal;		
		std::string strGrupoCanalID = stCanais.GrupoCanalID;
		std::string strRamal = stCanais.Ramal;
		std::string strSenha = stCanais.Senha;		

		strId.clear();
		strId.append("sip:");
		strId.append(strRamal);
		strId.append("@");
		strId.append( GetSipServerAddress() );

		strRegistrar.clear();
		strRegistrar.append("sip:");
		strRegistrar.append(GetSipServerAddress() );

		m_pluginSipPtr->AddAccount(strId, strRegistrar, strRamal , strSenha);			
		channelThrPtr.reset(	new ChannelThr(	this, 						
												m_pluginSipPtr,
												m_strDbFullPathName , 																	
												strRamal,
												boost::lexical_cast<std::string>(canalID),		
												strGrupoCanalID) );

		CInfoRamal canal(strRamal, boost::lexical_cast<std::string>(canalID) , channelThrPtr);
		m_infoRamalSet.insert(canal);

		canalID++;
		channelThrPtr->Start();
	}

	m_pluginSipPtr->app_main();
}


std::list<ST_CANAIS> SipServer::RecuperaCanais()
{
	std::list<ST_CANAIS> canaisList;
	//std::string sql = "select Canal, GrupoCanalID from Canais where Ativado = 1 and Habilitado = 1";
	std::string sql;
	sql.append("select C.Canal, C.GrupoCanalID , A.Ramal , A.Senha from Canais C inner join CanaisAnalogicos A ");
	sql.append(" on C.Canal = A.Canal where Ativado = 1 and Habilitado = 1 ");
	boost::shared_ptr<DataTable> dt = m_dbPtr->ExecuteQuery(sql.c_str());

	for (	DataTable::iterator it = dt->mi_dataRow.get<row>().begin() ; 
			it != dt->mi_dataRow.get<row>().end() ; 
			it++ )
	{
		DataRow row = (*it);		
		ST_CANAIS stCanais;

		std::string LoopCanal;
		std::string LoopGrupoCanalID;		
		std::string LoopRamal;
		std::string LoopSenha;

		for (	DataRow::iterator itC = row.mi_dataColumn.get<column>().begin() ; 
				itC != row.mi_dataColumn.get<column>().end() ; 
				itC++ )
		{
			DataColumn coluna = (*itC);

			if ( !coluna.name.compare("Canal"))
				LoopCanal = coluna.value;			
			else if ( !coluna.name.compare("GrupoCanalID"))
				LoopGrupoCanalID = coluna.value;
			else if ( !coluna.name.compare("Ramal"))
				LoopRamal = coluna.value;
			else if ( !coluna.name.compare("Senha"))
				LoopSenha = coluna.value;

			//if ( !LoopCanal.empty() && !LoopGrupoCanalID.empty() )
			if ( itC == (row.mi_dataColumn.get<column>().end() - 1 ) )
			{
				stCanais.Canal = LoopCanal;
				stCanais.GrupoCanalID = LoopGrupoCanalID;
				stCanais.Ramal = LoopRamal;
				stCanais.Senha = LoopSenha;

				canaisList.push_back(stCanais);
			}
		}
	}
	return canaisList;
}

///<summary>
/// L� da tabela Canais
///</summary>
int SipServer::GetNumCanaisHabilitados()
{	
	if ( !m_dbPtr->tableExists("Canais") )
		return 0;

	std::string sql = "SELECT count(*) as NumCanais from Canais where Habilitado = 1 and Ativado = 1";
	return m_dbPtr->ExecuteScalar(sql.c_str());
}

///<summary>
/// L� da tabela ParametrosGerais 
///</summary>
void SipServer::GetParametrosGerais()
{

}


std::string SipServer::GetDtmf()
{
	return "";
	//return m_Dtmf;
}

void SipServer::ClearDtmf()
{
	//m_Dtmf.clear();
}

void SipServer::OnIncomingCall(pjsua_call_info callInfo)
{
	boost::mutex::scoped_lock lock(m_mutex);

	pj_str_t pjStrLocalInfo = callInfo.local_info;
	std::string strLocalInfo;
	std::string Ramal;
	strLocalInfo.append( pjStrLocalInfo.ptr , pjStrLocalInfo.slen );	
	Ramal = strLocalInfo.substr( strLocalInfo.find(":") + 1 , strLocalInfo.find("@") - strLocalInfo.find(":") - 1 );
		
	ramal_info_by_ramal& ramal_index = m_infoRamalSet.get<tagRamal>();
	ramal_info_by_ramal::iterator it = ramal_index.find(Ramal);	
	
	boost::shared_ptr<ChannelThr> p_ChannelThr;
	if ( it != ramal_index.end() )
	{
		CInfoRamal ramalInfo = (*it);
		ramalInfo.call_info = callInfo;
		m_infoRamalSet.replace(it , ramalInfo);

		p_ChannelThr = ramalInfo.pThrChannel;		
		p_ChannelThr->SetCallInfo( callInfo );
		p_ChannelThr->NotifyAll();
	}
	
}

 bool SipServer::FindCallInfo(std::string strRamal, pjsua_call_info & call_info)
{	
	bool found = false;
	boost::mutex::scoped_lock lock(m_mutex);

	ramal_info_by_ramal& ramal_index = m_infoRamalSet.get<tagRamal>();
	ramal_info_by_ramal::iterator it = ramal_index.find(strRamal);
	
	boost::shared_ptr<ChannelThr> p_ChannelThr;
	if ( it != ramal_index.end() )
	{
		CInfoRamal ramalInfo = (*it);
		call_info = ramalInfo.call_info;
		found = true;
	}
	
	return found;
}



void SipServer::OnPlayEof(pjmedia_port *port, std::string Ramal)
{
	//m_condWailPlayEnd.notify_all();
}

void SipServer::OnDtmf(int call_id, int dtmf)
{
	//SetDtmf(dtmf);
}

std::string SipServer::GetSipServerAddress()
{
	return "172.16.32.25";	
}


