#pragma once

#include "IDatabase.h"
#include "pluginSip.h"

#define DOMINGO		0
#define SEGUNDA		1
#define	TERCA		2
#define QUARTA		3
#define	QUINTA		4
#define SEXTA		5
#define SABADO		6

#define UDP_INITIAL_PORT	5060
#define RTP_INITIAL_PORT	4000


typedef struct PERFIL_DIAS_DA_SEMANA
{
	std::string perfil_segunda;
	std::string perfil_terca;
	std::string perfil_quarta;
	std::string perfil_quinta;
	std::string perfil_sexta;
	std::string perfil_sabado;
	std::string perfil_domingo;
} ST_PERFIL_DIAS_DA_SEMANA;

typedef struct CALENDARIOCANAIS
{
	std::string Dia;
	std::string GrupoCanalID;
	std::string PerfilID;
} ST_CALENDARIOCANAIS;


class GROUPFILES
{
public:
	int ordemGrupo;
	int ordemArquivo;
	std::string fullFilePathName;
};

struct ordemGrupo{};
struct ordemArquivo{};
typedef boost::multi_index_container
<
	GROUPFILES,
	indexed_by
	<		
		ordered_unique
		<
			composite_key
			<
				GROUPFILES,
				member<GROUPFILES,int,&GROUPFILES::ordemGrupo>,
				member<GROUPFILES,int,&GROUPFILES::ordemArquivo>
			>
		>
	>
> GROUPFILES_SET;

class ChannelThr
{
public:	

	ChannelThr(void);
	
	ChannelThr(boost::shared_ptr< CPluginSip >	pluginSipPtr , std::string dbName , std::string ramal , std::string	id, std::string grupoCanalID);
	ChannelThr(	ISipServer * parent , boost::shared_ptr< CPluginSip >	pluginSipPtr, std::string dbName , std::string ramal , std::string	id, std::string grupoCanalID );
					
	~ChannelThr(void);

	boost::shared_ptr< CPluginSip >	m_pluginSipPtr;		
	ISipServer * m_pSipServer;
	boost::shared_ptr< IDatabase >	m_dbPtr;
	boost::shared_ptr< boost::thread > m_ChannelThreadPtr;
	volatile bool		m_bStop;	
	boost::mutex		m_mutex;
	boost::condition	m_cond;
	boost::condition	m_condWaitIncommingCall;
	boost::condition	m_condWaitRecordEnd;
	boost::condition	m_condWailPlayEnd;
	pj_thread_t *		m_thread;
	pj_pool_t			* m_pool;

	void SetCallInfo(pjsua_call_info call_info);
	void NotifyAll();	
	
	void Start();
	void Stop();	
	static void Run(ChannelThr * pThis);

	void SetDtmf(int dtmf);
	std::string Today();
	
public:
	int			m_thrNum;
	std::string m_strCanalID;
	std::string m_strGrupoCanalID;
	std::string m_strRamal;
	std::string m_strSenha;
	std::string m_strPerfilIDAtual;
	std::string m_strScriptIDAtual;
	std::string m_strBlocoID;
	
	void CarregaCalendarioCanais();
	ST_PERFIL_DIAS_DA_SEMANA m_PerfilDaSemana;

	void Init();
	void CarregaFromRecord();
	void CarregaGroupFiles();
	
	void CarregaGruposCanais( );
	bool CalendarioCanaisContains(std::string strPerfilToFind , std::list<ST_CALENDARIOCANAIS> calendarioCanaisList );
	void DeterminaPerfilID();
	void DeterminaScriptID();
	
	void CarregaBlocoInicial();
	std::string RecuperaTool(std::string strBlocoID);
	void AnalisaTool(std::string strTool);
	void DeterminaProximoBloco(std::string strTool);	
	void AnswerCall();
	void SetWaitForPlays(int num);
	void PlayFile(std::string strCanalID , std::string strRamal , std::string fileToPlay);
	void RecordFile(			std::string strCanalID , 
								std::string strRamal ,
								std::string strFileFullPathName,
								std::string strTempoGravacao,
								std::string strTonsTerminacao,
								std::string strSilencioMax);
	pj_status_t		ThreadCreate(	pj_pool_t	* pool , std::string strName , pj_thread_proc *proc, 
									void *arg, pj_size_t stack_size, 
									unsigned flags,
									pj_thread_t **ptr_thread);
	void ThreadDestroy();
	pj_status_t	    ResumeThread(pj_thread_t *ptr_thread);
	PJ_DEF(pj_status_t) ThreadRegister ( std::string strThreadName, pj_thread_desc desc, pj_thread_t **ptr_thread);
	pj_pool_t *	PoolCreate(std::string strName, int size);

	CInfoCanal DetInfoCanalByCallID(int paramCallID);
	CInfoCanal DetInfoCanalByRamal(std::string Ramal);

	std::list<ST_CALENDARIOCANAIS> m_calendarioCanaisList;
	std::string m_strTempoGravacao;
	std::string m_strTonsTerminacao;
	std::string m_strVariavel;
	std::string m_strSilencioMax;
	std::string m_Dtmf;
	

	GROUPFILES_SET m_GroupFilesSet;
	pjsua_call_info m_callInfo;		
	std::string GetSipServerAddress();
};

struct tagRamal{};
struct tagCanalID{};

class CInfoRamal
{
public:
	CInfoRamal() {}
	CInfoRamal(	std::string ramal,
				boost::shared_ptr<ChannelThr>  pChan) 
	{ 		
		Ramal = ramal;
		pThrChannel = pChan;
	}
	CInfoRamal(	std::string ramal,
				std::string canalID,
				boost::shared_ptr<ChannelThr>  pChan) 
	{ 		
		Ramal = ramal;
		strCanalID = canalID;
		pThrChannel = pChan;
	}
	CInfoRamal(	std::string ramal,
				std::string canalID,
				pjsua_call_info ci,
				boost::shared_ptr<ChannelThr>  pChan) 
	{ 		
		Ramal = ramal;		
		strCanalID = canalID;
		call_info = ci;
		pThrChannel = pChan;
	}

	std::string strCanalID;
	std::string Ramal;
	std::string strPassword;	
	pjsua_call_info call_info;
	boost::shared_ptr<ChannelThr> pThrChannel;
};

typedef boost::multi_index_container
<
	CInfoRamal,
	indexed_by
	<
		ordered_unique<tag<tagRamal>, member<CInfoRamal, std::string, &CInfoRamal::Ramal> >,
		ordered_unique<tag<tagCanalID>, member<CInfoRamal, std::string, &CInfoRamal::strCanalID> >
	>
> INFO_RAMAL_SET;

typedef INFO_RAMAL_SET::index<tagRamal>::type ramal_info_by_ramal;
typedef INFO_RAMAL_SET::index<tagCanalID>::type ramal_info_by_CanalID;