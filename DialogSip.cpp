// DialogSip.cpp : implementation file
//

#include "stdafx.h"
#include "DialogSip.h"


// DialogSip dialog

IMPLEMENT_DYNAMIC(DialogSip, CDialog)

DialogSip::DialogSip(CWnd* pParent /*=NULL*/)
	: CDialog(DialogSip::IDD, pParent)
{

}

DialogSip::~DialogSip()
{
}

void DialogSip::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DialogSip, CDialog)
	ON_BN_CLICKED(IDC_BTN_START, &DialogSip::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, &DialogSip::OnBnClickedBtnStop)
END_MESSAGE_MAP()


// DialogSip message handlers

void DialogSip::OnBnClickedBtnStart()
{
	// TODO: Add your control notification handler code here
}

void DialogSip::OnBnClickedBtnStop()
{
	// TODO: Add your control notification handler code here
}
