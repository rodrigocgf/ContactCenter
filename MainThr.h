#pragma once
#include "SipServer.h"

class MainThr
{
private:
	boost::shared_ptr< boost::thread > m_mainThreadPtr;
	volatile bool		m_bStop;	
	boost::mutex		m_mutex;
	boost::condition	m_cond;
	boost::shared_ptr<SipServer> m_sipServer;	

	std::string m_strDbFullPathName;
public:
	MainThr(void);
	~MainThr(void);

	void Start();
	void Stop();
	void Run();
};
