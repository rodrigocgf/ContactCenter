#include "StdAfx.h"
#include "MainThr.h"

#  include <winsock2.h>

MainThr::MainThr(void)
{	
	m_bStop = false;
	
	m_strDbFullPathName.assign("C:\\Users\\rodrigof\\Projects\\URA\\Tags\\GAT_CSHARP\\bin\\Debug\\Database\\ContactCenter.Sqlite3");
	m_sipServer.reset(new SipServer(m_strDbFullPathName));		
	m_sipServer->Start();
	m_mainThreadPtr.reset ( new boost::thread(boost::bind(&MainThr::Run, this ) ) );	
}

MainThr::~MainThr(void)
{
}

void MainThr::Start()
{	
	printf("MainThr::Start() \r\n");
	printf("Starting main thread...\r\n");
	
	m_mainThreadPtr->start_thread();
	m_mainThreadPtr->join();
}

void MainThr::Stop()
{	
	m_bStop = true;
	m_cond.notify_all();	
}

void MainThr::Run()
{
	while ( !m_bStop )	
	{		
		/*try
		{
			boost::this_thread::sleep(boost::posix_time::milliseconds(500));
		}
		catch(boost::exception &ex)
		{
			std::string strDbg = "1";
		}*/		
		
		boost::mutex::scoped_lock lk(  m_mutex );		
		m_cond.wait( lk );	
		lk.unlock();		
	}		
}