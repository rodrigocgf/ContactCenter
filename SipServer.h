#pragma once

//#include "pluginSip.h"
#include "IDatabase.h"
#include "ChannelThr.h"

class SipServer;


//struct tagRamal{};
//struct tagCanalID{};

//class CInfoRamal
//{
//public:
//	CInfoRamal() {}
//	CInfoRamal(	std::string ramal,
//				boost::shared_ptr<ChannelThr>  pChan) 
//	{ 		
//		Ramal = ramal;
//		pThrChannel = pChan;
//	}
//	CInfoRamal(	std::string ramal,
//				std::string canalID,
//				boost::shared_ptr<ChannelThr>  pChan) 
//	{ 		
//		Ramal = ramal;
//		strCanalID = canalID;
//		pThrChannel = pChan;
//	}
//	CInfoRamal(	std::string ramal,
//				std::string canalID,
//				pjsua_call_info ci,
//				boost::shared_ptr<ChannelThr>  pChan) 
//	{ 		
//		Ramal = ramal;		
//		strCanalID = canalID;
//		call_info = ci;
//		pThrChannel = pChan;
//	}
//
//	std::string strCanalID;
//	std::string Ramal;
//	std::string strPassword;	
//	pjsua_call_info call_info;
//	boost::shared_ptr<ChannelThr> pThrChannel;
//};
//
//typedef boost::multi_index_container
//<
//	CInfoRamal,
//	indexed_by
//	<
//		ordered_unique<tag<tagRamal>, member<CInfoRamal, std::string, &CInfoRamal::Ramal> >,
//		ordered_unique<tag<tagCanalID>, member<CInfoRamal, std::string, &CInfoRamal::strCanalID> >
//	>
//> INFO_RAMAL_SET;

//typedef INFO_RAMAL_SET::index<tagRamal>::type ramal_info_by_ramal;
//typedef INFO_RAMAL_SET::index<tagCanalID>::type ramal_info_by_CanalID;

typedef struct CANAIS
{
	std::string Canal;
	std::string GrupoCanalID;
	std::string Ramal;
	std::string Senha;
} ST_CANAIS;

class SipServer : public ISipServer
{
public:
	SipServer(void);
	SipServer(std::string dbName);
	~SipServer(void);

	int Start();
	boost::shared_ptr< CPluginSip >	m_pluginSipPtr;
	boost::shared_ptr<IDatabase>	m_dbPtr;

	std::string GetDtmf();
	void ClearDtmf();
	void OnIncomingCall(pjsua_call_info callInfo);
	bool FindCallInfo(std::string strRamal, pjsua_call_info & call_info);
	//void OnIncomingCall(std::string Ramal);
	void OnPlayEof(pjmedia_port *port, std::string Ramal);
	void OnDtmf(pjsua_call_id call_id, int dtmf);

	std::string GetSipServerAddress();
	//void AnswerCall(pjsua_call_info canalInfo);
	//void SetWaitForPlays(int num);
	//void PlayFile(std::string strCanalID , std::string strRamal , std::string fileToPlay);	
	/*void RecordFile(	std::string strCanalID , 
						std::string strRamal ,
						std::string strFileFullPathName,
						std::string strTempoGravacao,
						std::string strTonsTerminacao,
						std::string strSilencioMax);*/

	//pj_pool_t *		PoolCreate(std::string strName, int size);
	/*pj_status_t		ThreadCreate(	pj_pool_t	* pool , std::string strName , pj_thread_proc *proc, 
											void *arg, pj_size_t stack_size, 
											unsigned flags,
											pj_thread_t **ptr_thread);*/
	/*pj_status_t	    ResumeThread(pj_thread_t *ptr_thread);
	PJ_DEF(pj_status_t) ThreadRegister ( std::string strThreadName, pj_thread_desc desc, pj_thread_t **ptr_thread);*/
private:
	void GetParametrosGerais();
	int GetNumCanaisHabilitados();
	void LaunchChannelsThreads(int numCanais);
	boost::mutex  m_mutex;

	
	std::map<std::string, boost::shared_ptr<ChannelThr> > threadsMap;
	std::string m_strDbFullPathName;	
	std::list<ST_CANAIS> RecuperaCanais();
	
	INFO_RAMAL_SET m_infoRamalSet;
};
