#include "StdAfx.h"
#include "DlgSip.h"

LONG WINAPI WndProc( HWND hWnd, UINT msg,WPARAM wParam, LPARAM lParam );

#define MAIN_WINDOW		0
#define BTN_START_CLICK	1
#define BTN_STOP_CLICK	2

DlgSip::DlgSip(void)
{
	m_mainThrPtr.reset(new MainThr());
}

DlgSip::~DlgSip(void)
{
}

int DlgSip::Show()
{
	char szAppName[] = "Contact Center";
	char szTitle[]="Contact Center";
	WNDCLASS wc;
	MSG      msg;
	HWND     hWnd;
	HWND	 hButtonStart , hButtonStop;

	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   =  (WNDPROC)WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;	
	wc.hIcon         = NULL;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = szAppName;

	RegisterClass( &wc );

	HINSTANCE hInst = GetModuleHandle (0);		

	hWnd = CreateWindowEx( 0, szAppName, szTitle, 
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		300, 300,
		//CW_USEDEFAULT, CW_USEDEFAULT,		
		300,  150, NULL, (HMENU)MAIN_WINDOW, hInst, (void *)this );

	//hWnd = CreateWindow(  szAppName, szTitle, 
	//	WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
	//	300, 300,
	//	//CW_USEDEFAULT, CW_USEDEFAULT,
	//	300,  150, NULL, (HMENU)MAIN_WINDOW , hInst, (void *)this );

	//If window could not be created, return zero
	if ( !hWnd )
	{
		std::string strError = boost::lexical_cast<std::string> ( GetLastError() );
		return( 0 );
	}
	
	hButtonStart = CreateWindow("BUTTON" , "START", WS_VISIBLE | WS_CHILD | WS_BORDER , 
								30, 30 , 90, 30, hWnd,(HMENU)BTN_START_CLICK,NULL,(void *)this);
	if ( !hButtonStart )
	{
		std::string strError = boost::lexical_cast<std::string> ( GetLastError() );
		return( 0 );
	}

	hButtonStop = CreateWindow( "BUTTON" , "STOP", WS_VISIBLE | WS_CHILD | WS_BORDER , 
								150, 30 , 90, 30, hWnd,(HMENU)BTN_STOP_CLICK,NULL,(void *)this);
	if ( !hButtonStop )
	{
		std::string strError = boost::lexical_cast<std::string> ( GetLastError() );
		return( 0 );
	}

	ShowWindow( hWnd, 1 );
	UpdateWindow( hWnd ); 

	while (GetMessage(&msg, // message structure
		NULL,       // handle of window receiving
		// the message
		0,          // lowest message id to examine
		0))         // highest message id to examine
	{
		TranslateMessage( &msg ); // Translates messages
		DispatchMessage( &msg );  // then dispatches
	}

	return( msg.wParam );
}



LONG WINAPI WndProc( HWND hWnd, UINT msg,
					WPARAM wParam, LPARAM lParam )
{	
	DlgSip * p_parent = (DlgSip *)(lParam);
	boost::shared_ptr<MainThr> p_mainThr = p_parent->m_mainThrPtr;
	switch(msg)
	{
	case WM_CLOSE:
		PostQuitMessage(0);
		break;
	case WM_COMMAND:
		if(LOWORD(wParam)==BTN_START_CLICK)
		{
			if ( p_mainThr.get() !=  NULL )
				p_mainThr->Start();			
			//MessageBox(hWnd, TEXT("Button START Pressed"), TEXT(""), 0);
		}

		if(LOWORD(wParam)==BTN_STOP_CLICK)
		{
			if ( p_mainThr != NULL )
				p_mainThr->Stop();
			
		}
		
		break;
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	return 0;
}
